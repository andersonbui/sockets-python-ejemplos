#!/usr/bin/env python

#Se importa el modulo
import socket
import ssl
import multiprocessing


def process_request(sock):
    try:
        while( true ):
            sock.send('hola cliente')
            print(sock.recv(1024))
    
    finally:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()

#instanciamos un objeto para trabajar con el socket
ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

"""
Cuando en una ejecucion anterior se deja el socket en un estado de timeout (TIME_WAIT), el 
flag socket.SO_REUSEADDR permite indicar al Kernel que sera posible volver a utilizar el
socket sin tener que esperar a que el timeout expire.
"""
ser.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

#Puerto y servidor que debe escuchar
ser.bind(("", 8050))

#Aceptamos conexiones entrantes con el metodo listen. Por parametro las conexiones simutaneas.
ser.listen(5)

while True:

    #Instanciamos un objeto cli (socket cliente) para recibir datos
    # cli, addr = ser.accept()
    cli, (client_ip, client_port) = ser.accept()

    """
	Envolvemos el socket dentro del contexto de seguridad del protocolo TLSv1.
	Se especifica que se trata del lado servidor y por lo tanto se define la 
	ruta hacia los certificados que deben utilizarse. 
	"""
    conn = ssl.wrap_socket(cli, server_side = True, certfile = "./certificados/server.crt", 
        keyfile="./certificados/server.key", ssl_version=ssl.PROTOCOL_TLSv1)

    subprocess = multiprocessing.Process(target=process_request, args=(conn,))
    subprocess.start()

#Cerramos la instancia del socket cliente y servidor
ser.close()
