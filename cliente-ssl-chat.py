#!/usr/bin/env python

import socket
import ssl
import multiprocessing
import threading
import pickle
import sys
import time
import os
import psutil

class Cliente(object):

    def __init__(self, host='localhost', port=8050):
        self.host = host
        self.port = port
        self.conn = None

        self.intentar_conexion()

        if self.conn:
            while(True):
                try:
                    msg = input("->")
                    if(msg != 'salir'):
                        print("ret envio: ",self.send_msg(msg))
                    else:
                        print("saliendo del servidor")
                        self.conn.shutdown(socket.SHUT_RDWR)
                        self.conn.close()
                        sys.exit()
                except socket.timeout:
                    print("termino pir fuera de conexion")

    def intentar_conexion(self):
        self.conectado = False
        while(not(self.conectado)):
            self.conectado = True
            time.sleep(5)
            try:
                self.conexion()
            except Exception as e:
                self.conectado = False
                print(str(e))
                pass


    def conexion(self):
        """
        Se crea un objeto del tipo SSLContext, el cual recibe por parametro el protocolo que
        se desea utilizar en la negociacion de la seguridad para el establecimiento de la conexion.
        """
        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)

        #Creacion de un objeto socket (lado cliente)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # sock.setblocking(False)

        """
        Envuelve el socket dentro del contexto de seguridad especificado anteriormente.
        Retorna un objecto del tipo SSLSocket.
        """
        self.conn = context.wrap_socket(sock)
        # self.conn = sock
        #Conexion con el servidor. Parametros: IP (puede ser del tipo 192.168.1.1 o localhost), Puerto
        self.conn.connect((self.host, self.port))
        print("Conectado al servidor")

        subprocess = threading.Thread(target=self.process_request, args=())
        # subprocess = multiprocessing.Process(target=self.process_request, args=())
        subprocess.daemon = True
        subprocess.start()


    def process_request(self):
        print("procesando peticion")
        #Creamos un bucle para retener la conexion
        while True:
            time.sleep(1)
            try:
                #obj.send(mens)
                recibido = self.conn.recv(1024)
                if recibido:
                    print("server dice:", pickle.loads(recibido))
                else:
                    print("server :", pickle.loads(recibido))

            except Exception as e:
                print("exception: ", e)
                break

        #Cerramos la instancia del objeto servidor
        self.conn.close()
        print("Conexion cerrada")
        self.restart_program()

    def send_msg(self, cuerpo) -> bool:
        print("enviando mensaje: "+cuerpo)
        data = pickle.dumps(cuerpo, 2)
        sent_length = self.conn.send(data) # cuando loads -> python2
        # self.conn.send(pickle.dumps(cuerpo, 3)) # 
        print("enviando data: ",(sent_length))

        return sent_length

    def restart_program(self):
        """Restarts the current program, with file objects and descriptors
        cleanup
        """

        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                os.close(handler.fd)
        except Exception as e:
            print(e)

        python = sys.executable
        os.execl(python, python, *sys.argv)
        print("Se reinicio el programa")


# cliente = Cliente()
cliente = Cliente('192.168.50.149')