#!/usr/bin/env python

#Variables
host = '192.168.50.149'
port = 8050
#Se importa el modulo
import socket
import ssl

"""
Se crea un objeto del tipo SSLContext, el cual recibe por parametro el protocolo que
se desea utilizar en la negociacion de la seguridad para el establecimiento de la conexion.
"""
context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)

#Creacion de un objeto socket (lado cliente)
obj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

"""
Envuelve el socket dentro del contexto de seguridad especificado anteriormente.
Retorna un objecto del tipo SSLSocket.
"""
conn = context.wrap_socket(obj)

#Conexion con el servidor. Parametros: IP (puede ser del tipo 192.168.1.1 o localhost), Puerto
conn.connect((host, port))
print("Conectado al servidor")

#Creamos un bucle para retener la conexion
while True:
    #Instanciamos una entrada de datos para que el cliente pueda enviar mensajes
    mens = input("Mensaje desde Cliente a Servidor >> ")
    #Con el metodo send, enviamos el mensaje
    conn.send(mens.encode('ascii'))
    #obj.send(mens)
    recibido = conn.recv(1024)
    print(recibido)

#Cerramos la instancia del objeto servidor
conn.close()

#Imprimimos la palabra Adios para cuando se cierre la conexion
print("Conexion cerrada")