#!/usr/bin/env python

#Variables
host = '192.168.50.149'
port = 8050
#Se importa el modulo
import socket

#Creacion de un objeto socket (lado cliente)
obj = socket.socket()

#Conexion con el servidor. Parametros: IP (puede ser del tipo 192.168.1.1 o localhost), Puerto
obj.connect((host, port))
print("Conectado al servidor")

#Creamos un bucle para retener la conexion
while True:
    #Instanciamos una entrada de datos para que el cliente pueda enviar mensajes
    mens = input("Mensaje desde Cliente a Servidor >> ")
    #Con el metodo send, enviamos el mensaje
    obj.send(mens.encode('ascii'))
    #obj.send(mens)
    recibido = obj.recv(1024)
    print(recibido)

#Cerramos la instancia del objeto servidor
obj.close()

#Imprimimos la palabra Adios para cuando se cierre la conexion
print("Conexion cerrada")