#!/usr/bin/env python

#Se importa el modulo
import socket
import ssl
import multiprocessing
import threading
import pickle
import sys
import time
import select

class Servidor(object):

    def __init__ (self, host='', port=8050):
        # Arreglo de clientes
        self.clientes = []

        #instanciamos un objeto para trabajar con el socket
        self.ser = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        """
        Cuando en una ejecucion anterior se deja el socket en un estado de timeout (TIME_WAIT), el 
        flag socket.SO_REUSEADDR permite indicar al Kernel que sera posible volver a utilizar el
        socket sin tener que esperar a que el timeout expire.
        """
        self.ser.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        #Puerto y servidor que debe escuchar
        self.ser.bind((host, port))

        #Aceptamos conexiones entrantes con el metodo listen. Por parametro las conexiones simutaneas.
        self.ser.listen(5)

        # bloqueante para que se quede esperando a una nueva conexion
        # self.ser.setblocking(False) 

        # hilo procesar peticiones
        subprocess = threading.Thread(target=self.process_request, args=())
        subprocess.daemon = True
        subprocess.start()

        # hilo aceptar peticiones
        aceptarsubprocess = threading.Thread(target=self.aceptarConexiones, args=())
        aceptarsubprocess.daemon = True
        aceptarsubprocess.start()

        # enviar mensajes
        while(True):
            msg = input('->')
            if( msg == 'salir' ):
                self.ser.shutdown( socket.SHUT_RDWR )
                self.ser.close()
                sys.exit()
            else:
                self.send_mensage( msg )
                pass


    def aceptarConexiones(self):
        print(" -- escuchando clientes -- ")

        while True:
            time.sleep(1)
            try:

                #Instanciamos un objeto cli (socket cliente) para recibir datos
                cli, (client_ip, client_port) = self.ser.accept()
                print(" aceptarConexiones: ip: " + str(client_ip) + " | port: " + str(client_port) )

                # cli.setblocking(False)
                print("clientes 0: "+ str(client_ip))

                """
                Envolvemos el socket dentro del contexto de seguridad del protocolo TLSv1.
                Se especifica que se trata del lado servidor y por lo tanto se define la 
                ruta hacia los certificados que deben utilizarse. 
                """
                # conn = cli
                conn = ssl.wrap_socket(cli, server_side = True, certfile = "./certificados/server.crt", 
                    keyfile="./certificados/server.key", ssl_version = ssl.PROTOCOL_TLSv1)

                # no bloqueante para que este monitoreando los clientes
                conn.setblocking(False)
                print("clientes 1: "+ str(client_ip))

                self.clientes.append(conn)
                print("clientes 2: "+ str(len(self.clientes)))

            except Exception as e:
                print("Oops: ", sys.exc_info()[0], " occured", str(e))
                pass
            #     print(" aceptarConexiones exception: " + str(e))
                
        #Cerramos la instancia del socket cliente y servidor
        # self.ser.close()

    def process_request(self):
        print(" -- cliente conectado -- ")
        
        while( True ):
            # print("clientes: "+ str(len(self.clientes)))
            if(len(self.clientes) > 0):

                potential_readers = self.clientes
                potential_writers = []
                potential_errs = []
                timeot = 15
                ready_to_read, ready_to_write, in_error = select.select(
                        potential_readers,
                        potential_writers,
                        potential_errs,
                        timeot
                    )
                print("-")
                for c in ready_to_read:
                    try:
                        data =  c.recv(1024)
                        if(data == b''):
                            c.close()
                            self.clientes.remove(c)
                            print("termino: ", str(c))
                        if(data):
                            data = pickle.loads(data)
                            print(" -- llego mensaje: " , data)
                            # data = pickle.dumps(data)
                            self.send_mensage(data, c)

                    except Exception as e:
                        pass
                        print(" process_request exception: " + str(e))
                        # c.shutdown(socket.SHUT_RDWR)
                        # c.close()
            else:
                time.sleep(10)

    def send_mensage(self, data, cliente = None):

        print(" enviando mensaje: " + str(data))
        data = pickle.dumps(data)
        for c in self.clientes:
            try:
                if(c != cliente):
                    c.send(data)
            except Exception as err:
                print(" process_request exception: " + str(err))
                self.clientes.remove(c)
                c.shutdown(socket.SHUT_RDWR)
                c.close()

cliente = Servidor()